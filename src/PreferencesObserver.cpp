/// @file

#include "PreferencesObserver.hpp"

// do nothing by default
void PreferencesObserver::preferenceChanged_geniusUpdateArtist(bool)  { }
void PreferencesObserver::preferenceChanged_geniusUpdateAlbum(bool)  { }
void PreferencesObserver::preferenceChanged_geniusUpdateTitle(bool)  { }
void PreferencesObserver::preferenceChanged_geniusUpdateYear(bool)  { }
void PreferencesObserver::preferenceChanged_geniusUpdateGenre(bool)  { }
void PreferencesObserver::preferenceChanged_geniusUpdateFilename(bool)  { }

void PreferencesObserver::preferenceChanged_geniusSearchWithArtist(bool)  { }
void PreferencesObserver::preferenceChanged_geniusSearchWithAlbum(bool)  { }
void PreferencesObserver::preferenceChanged_geniusSearchWithTitle(bool)  { }
void PreferencesObserver::preferenceChanged_geniusSearchWithYear(bool)  { }
void PreferencesObserver::preferenceChanged_geniusSearchWithGenre(bool)  { }
void PreferencesObserver::preferenceChanged_geniusSearchWithFilename(bool)  { }

void PreferencesObserver::preferenceChanged_geniusAlwaysUseFirstHit(bool)  { }
