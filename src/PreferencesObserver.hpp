/// @file

#pragma once

class Preferences;

class PreferencesObserver  {
    protected:
        virtual void preferenceChanged_geniusUpdateArtist(bool value);
        virtual void preferenceChanged_geniusUpdateAlbum(bool value);
        virtual void preferenceChanged_geniusUpdateTitle(bool value);
        virtual void preferenceChanged_geniusUpdateYear(bool value);
        virtual void preferenceChanged_geniusUpdateGenre(bool value);
        virtual void preferenceChanged_geniusUpdateFilename(bool value);

        virtual void preferenceChanged_geniusSearchWithArtist(bool value);
        virtual void preferenceChanged_geniusSearchWithAlbum(bool value);
        virtual void preferenceChanged_geniusSearchWithTitle(bool value);
        virtual void preferenceChanged_geniusSearchWithYear(bool value);
        virtual void preferenceChanged_geniusSearchWithGenre(bool value);
        virtual void preferenceChanged_geniusSearchWithFilename(bool value);

        virtual void preferenceChanged_geniusAlwaysUseFirstHit(bool value);

    friend Preferences;
};
