#include "Exception.hpp"
#include "qmessagebox.h"
#include <iostream>

#include "qstring.h"
using namespace std;

TException::TException(QString _msg)  { msg=_msg; }
void TException::show()  {
    QMessageBox box;
    box.setIcon(QMessageBox::Critical);
    box.setText(msg);
    box.exec();
    cerr << "An exception occured:"<<endl<<msg.toUtf8().data()<<endl;
}
const char *TException::what() const noexcept  {
    return msg.toStdString().c_str();
}

void fErrorMsg(QString msg)  {
    QMessageBox box;
    /// @todo function name would not be resolved correctly
    box.setText("ERROR:\n\n"+QString("Error in function   ")+QString(__PRETTY_FUNCTION__)+QString("   :\n")+QString(msg));
    box.exec();
}

void INFO_MESSAGE(QString msg)  {
    QMessageBox box;
    box.setIcon(QMessageBox::Information);
    box.setText(QString(msg));
    box.exec();
}
