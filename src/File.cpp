#include "File.hpp"
#include "src/util/Exception.hpp"
#include "QTreeWidgetItem"
#include "QTreeWidget"
#include "QProcess"
#include "iostream"
#include "qdir.h"
using namespace std;

std::vector<TFile*> TFile::allFiles;
QString TFile::working_dir;

TFile::TFile(QString _fn, QTreeWidget *parent)
    :  QTreeWidgetItem(parent), accept_edit(false)  {
    fn = _fn.right(_fn.size()-working_dir.size());
    subdir = fn.left(fn.lastIndexOf("/")+1);  /// @todo magic string
    fn = fn.right(fn.size() - subdir.size());
    refresh();
    allFiles.push_back(this);
}
TFile::~TFile()  {
    const auto itr = std::find(allFiles.begin(), allFiles.end(), this);
    if(itr != allFiles.end())
        allFiles.erase(itr);
}

void TFile::setTag(QString tagname, int column, QString *variable)  {
    QProcess p;
    QString cmd;
    if(text(column).size()!=0)
        cmd = "id3v2 \""+getAbsoluteFn()+"\" --id3v2-only --"+tagname+" \""+text(column)+"\"";
    else
        cmd = "id3v2 \""+getAbsoluteFn()+"\" --"+tagname+" \" \"";
    p.start(cmd);
    if(! p.waitForFinished(1000))  {
        ERROR_MSG("cannot set "+tagname);
        setText(column, *variable);
        return ;
    }
    QString err = p.readAllStandardError();
    if(err.size()!=0) {
        ERROR_MSG("cannot set "+tagname+":\n"+err);
        setText(column, *variable);
        return ;
    }
    *variable = text(column);
}

void TFile::setFn(QString _fn)  {
    setText(1,_fn);  /// @todo magic number
}
void TFile::setArtist(QString _artist)  {
    setText(2,_artist);
}
void TFile::setAlbum(QString _album)  {
    setText(3,_album);
}
void TFile::setTitle(QString _title)  {
    setText(4,_title);
}
void TFile::setYear(QString _year)  {
    setText(5,_year);
}
void TFile::setGenre(QString _genre)  {
    setText(6,_genre);
}

QString TFile::getSubdir()  { return subdir; }
QString TFile::getFn()      { return fn;     }
QString TFile::getArtist()  { return artist; }
QString TFile::getAlbum()   { return album;  }
QString TFile::getTitle()   { return title;  }
QString TFile::getYear()    { return year;   }
QString TFile::getGenre()   { return genre;  }

QString TFile::getAbsoluteFn()  {
    return working_dir+subdir+fn;
}

QString TFile::getEnding()  {
    return fn.right(fn.size()-fn.lastIndexOf('.'));
}

void TFile::refresh()  {
    accept_edit = false;
    if(!QFile(getAbsoluteFn()).exists())  {
        delete this;
        return ;
    }
    QProcess p;
    p.start("id3v2 -l \""+getAbsoluteFn()+"\"");
    if(!p.waitForFinished(1000))
        ERROR("could not read ID3v2 tags from \""+getAbsoluteFn()+"\"");
    QString raw = p.readAllStandardOutput();
    raw.replace("\r\n","\n");

    artist = "";
    album  = "";
    title  = "";
    year   = "";
    genre  = "";

    QStringList tags = raw.split("\n");
    for(int i=0; i<tags.size(); i++)
        if(     tags[i].size() == 0
                || tags[i].startsWith("id3v2 tag info for",Qt::CaseInsensitive)
                || tags[i].startsWith("id3v1 tag info for",Qt::CaseInsensitive)
                || tags[i].indexOf("no id3v1 tag",0,Qt::CaseInsensitive)!=-1
                || tags[i].indexOf("no id3v2 tag",0,Qt::CaseInsensitive)!=-1
                )  {
            tags.removeAt(i);
            i--;
        }
    for(QString tag : tags)  {
        int delpos = tag.indexOf(":");
        if(delpos==-1 || delpos>tag.size()-2)
            ERROR("cannot parse ID3v2 output");

        QString name = tag.left(delpos);
            name = name.split(' ')[0];
        QString value = tag.right(tag.size()-delpos-2);
        if(value==" ")
            value = "";
        name = name.toUpper();
        if     (name=="TALB")
            album  = value;
        else if(name=="TPE1")
            artist = value;
        else if(name=="TIT2")
            title  = value;
        else if(name=="TCON")  {
            if(value.indexOf(" (") != -1)
                value = value.left(value.indexOf(" ("));
            genre  = value;
        }
        else if(name=="TDRC" || name=="TYER")
            year   = value;
        //else
            //ERROR_MSG("unknwon ID3 tag: \""+name+"\"");
            /// @todo actual ERROR
    }

    setText(0,subdir);  /// @todo magic numbers
    setText(1,fn);
    setText(2,artist);
    setText(3,album);
    setText(4,title);
    setText(5,year);
    setText(6,genre);
    setFlags(flags() | Qt::ItemIsEditable);

    //if(subdir=="done/")  {
        for(int i=0; i<treeWidget()->columnCount(); i++)  {
            /// @todo this does not work anymore in Qt 5.15
            /*QBrush brush = foreground(i);
            brush.setColor(  subdir!="done/"  ?  QColor(Qt::black)  :  QColor(180,180,180)  );   /// @todo magic numbers
            setForeground(i,brush);*/
            QFont f = font(i);
            f.setItalic(subdir=="done/");
            setFont(i,f);
        }
    //}

    accept_edit = true;
}
void TFile::edited(int column)  {
    if(!accept_edit)
        return;
    accept_edit = false;
    if(column == 0)  {
        if(! QDir(working_dir+text(0)).exists())  {
            ERROR_MSG("invalid path");
            setText(0,subdir);
        }
        else {
            QString newsubdir = text(0);
            if(newsubdir.size()!=0 && newsubdir[subdir.size()-1]!='/')
                newsubdir += "/";
            if(! QFile(getAbsoluteFn()).rename(working_dir+subdir+fn))  {
                ERROR_MSG("cannot move file");
                setText(0,subdir);
            }
            else {
                subdir = newsubdir;
                setText(0,subdir);
            }
        }
    }
    else if(column == 1)  {
        if(QFile(text(1)).exists())  {
            ERROR_MSG("A file with that name already exists.");
            setText(1,fn);
        }
        else  {
            QString newfn = text(1);
            if(QFileInfo(newfn).suffix() != QFileInfo(fn).suffix())
                newfn += "."+QFileInfo(fn).suffix();
            if(! QFile(getAbsoluteFn()).rename(working_dir+subdir+newfn))  {
                ERROR_MSG("cannot rename file to >"+newfn+"<");
                setText(1,fn);
            }
            else  {
                fn = newfn;
                setText(1,newfn);  // user's input may be manipulated above
            }
        }
    }
    else if(column == 2)  setTag("artist",2,&artist);
    else if(column == 3)  setTag("album" ,3,&album );  /// @todo cannot empty field
    else if(column == 4)  setTag("song"  ,4,&title );
    else if(column == 5)  setTag("year"  ,5,&year );
    else if(column == 6)  setTag("genre" ,6,&genre );  /// @todo expects number ?
    else
        NOT_IMPLEMENTED();
    accept_edit = true;
}

std::vector<TFile*> TFile::getAllFiles()  {
    return allFiles;
}
std::vector<TFile*> &TFile::getAllFilesByReference()  {
    return allFiles;
}
