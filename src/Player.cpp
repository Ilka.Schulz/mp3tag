/// @file

#include "Player.hpp"
#include "src/File.hpp"

Player::Player(std::vector<TFile *> &_files)
    : files(_files), currentlyPlayingFile(nullptr), loopMode(none), shuffleMode(false)  {
    QMediaPlayer::setNotifyInterval(100);
    connect(this, &QMediaPlayer::stateChanged, this, &Player::stateChanged);
    connect(this, &QMediaPlayer::positionChanged, this, &Player::positionChanged);
    connect(this, &QMediaPlayer::durationChanged, this, &Player::durationChanged);
}

void Player::selectFile(TFile *file)  {
    QMediaPlayer::setMedia(QUrl::fromLocalFile(file->getAbsoluteFn()));
    currentlyPlayingFile = file;
    for(PlayerObserver *observer : observers)  {
        observer->playerEvent_timeStampChanged(this);
        observer->playerEvent_durationChanged(this);
    }
}
void Player::start()  {
    QMediaPlayer::play();
    for(PlayerObserver *observer : observers)
        observer->playerEvent_started(this);
}
void Player::pause()  {
    QMediaPlayer::pause();
    for(PlayerObserver *observer : observers)
        observer->playerEvent_paused(this);
}
void Player::stop()  {
    QMediaPlayer::stop();
    for(PlayerObserver *observer : observers)
        observer->playerEvent_stopped(this);
}
void Player::setLoopMode(LoopMode loopMode)  {
    this->loopMode = loopMode;
    for(PlayerObserver *observer : observers)
        observer->playerEvent_loopModeChanged(this);
}
void Player::setShuffleMode(bool shuffle)  {
    this->shuffleMode = shuffle;
    for(PlayerObserver *observer : observers)
        observer->playerEvent_shuffleModeChanged(this);
}
void Player::setVolume(int volume)  {
    QMediaPlayer::setVolume(volume);
    for(PlayerObserver *observer : observers)
        observer->playerEvent_volumeChanged(this);
}
void Player::moveToTimeStamp(qint64 timeStamp)  {
    QMediaPlayer::setPosition(timeStamp);
}



TFile *Player::getCurrentlyPlayingFile()  {
    return currentlyPlayingFile;
}
Player::LoopMode Player::getLoopMode()  {
    return loopMode;
}
bool Player::getShuffleMode()  {
    return shuffleMode;
}
int Player::getVolume()  {
    return volume();
}



bool Player::isPlaying()  {
    return state() == State::PlayingState;
}
qint64 Player::getTimeStamp()  {
    return QMediaPlayer::position();
}
qint64 Player::getDuration()  {
    return QMediaPlayer::duration();
}


void Player::stateChanged(State state)  {
    if(state == StoppedState)
        /// @todo handle loop and shuffle
        ;
    if(state == PlayingState  || state == PausedState)
        ;
        /// @todo implement
}
void Player::positionChanged(qint64 position)  {
    for(PlayerObserver *observer : observers)
        observer->playerEvent_timeStampChanged(this);
}
void Player::durationChanged(qint64 position)  {
    for(PlayerObserver *observer : observers)
        observer->playerEvent_durationChanged(this);
}
