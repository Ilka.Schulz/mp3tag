#pragma once

#include <QMainWindow>
#include "qtreewidget.h"
#include "qfilesystemwatcher.h"
#include <map>
#include <QTimer>
#include "src/ui/PlayerWidget.hpp"
#include "src/GeniusLookupManager.hpp"

using namespace std;

class TFile;
class TTreeWidget;
class QGridLayout;
class QLabel;
class QLineEdit;
class QToolButton;


/// @todo use arrow keys to move playback position / change volume
/// @todo click on horizontal slider should result in jump to that position
class TMainWindow : public QMainWindow,  protected PlayerObserver  {

private:
    GeniusLookupManager geniusLookupManager;
    QTimer timer;
    QFileSystemWatcher fswatcher;  /// @todo on file name rename: update fswatcher
    // ui
    QGridLayout *grid;
        // path selection
            QLabel *pathLabel;
            QLineEdit *pathLineEdit;
            QToolButton *pathSelectionToolButton;
            QToolButton *pathConfirmationToolButton;
        // tree widget
        TTreeWidget *treeWidget;
        // player
        PlayerWidget *playerWidget;
    QLabel *label_StatusBar_Requests;
    QLabel *label_StatusBar_QueuedRequests;

public:
    explicit TMainWindow(QWidget *parent = nullptr);
    ~TMainWindow();

private slots:
    void pathSelectionToolButtonClickedEvent(bool checked);
    void pathLineEditEditedEvent(const QString &arg1);
    void pathLineEditReturnPressedEvent();
    void pathConfirmationToolButtonClickedEvent(bool checked);
    void treeWidgetCustomContextMenuRequestedEvent(const QPoint &pos);
    void treeWidgetItemChangedEvent(QTreeWidgetItem *item, int column);

    void on_actionPreferences_triggered();

private:
    void scanDir();
    void scanDir(QString dn, bool recursive=true);

private:
    // context menu
    void on_play_triggered();
    void on_play_with_vlc_triggered();
    void on_edit_triggered();
    void on_refresh_triggered();
    void on_moveToTrash_triggered();
    void on_moveToDone_triggered();
    void on_moveToWorkingDirectory_triggered();
    void on_switchToSubdir_triggered();
    void on_setAccordingFn_triggered();
    void on_parseFn_triggered();
    void on_findTagsOnline_triggered();

    // other events
    void on_fileChanged(const QString &path);
    void on_directoryChanged(const QString &path);
    void on_timeOut();

public:
    void selectPath(QString path);
};

