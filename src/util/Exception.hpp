#pragma once
#include <QString>
#include "qmessagebox.h"
using namespace std;

/**
 * @brief The TException class holds a message and should be the only class thrown.
 * Other classes may be derived from this one to be thrown, too.
 */
class TException  :  public exception
{
protected:
    QString msg;                                  ///< error message
public:
    TException(QString _msg);                     ///< constructor @param _msg error message
    void show();                                 ///< shows the error message to the user, eg over stderr or a message box
    virtual const char *what() const noexcept override;
};

void fErrorMsg(QString msg);


#define NOT_IMPLEMENTED()  throw TException(QString("Error: The function named   ")+QString(__PRETTY_FUNCTION__)+QString("   is not yet (fully) implemented."))
#define UNREACHABLE()      throw TException(QString("Error: The function named   ")+QString(__PRETTY_FUNCTION__)+QString("   reached code that is supposed to be unreachable."))
#define ERROR(msg)         throw TException(QString("Error in function\n\n")+QString(__PRETTY_FUNCTION__)+QString("  :\n\n")+QString(msg))
#define ERROR_MSG(msg)     fErrorMsg(msg)

void INFO_MESSAGE(QString msg);
