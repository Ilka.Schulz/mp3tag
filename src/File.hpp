#pragma once
#include "qstring.h"
#include "qtreewidget.h"
#include <QTreeWidgetItem>
#include <vector>
using namespace std;

class TFile  :  public QTreeWidgetItem  {
    private:
        static std::vector<TFile*> allFiles;

    public:
        static QString working_dir;  /// @todo wtf, why is this static?

    private:
        QString subdir;
        QString fn;
        QString artist, album, title, year, genre;

        bool accept_edit;
    public:
        TFile(QObject *parent);
        TFile(QString _fn, QTreeWidget *parent);
        ~TFile();

    private:
        void setTag(QString tagname, int column, QString *variable);

    public:
        void setFn(QString _fn);
        void setArtist(QString _artist);
        void setAlbum(QString _album);
        void setTitle(QString _title);
        void setYear(QString _year);
        void setGenre(QString _genre);

        QString getSubdir();
        QString getFn();
        QString getArtist();
        QString getAlbum();
        QString getTitle();
        QString getYear();
        QString getGenre();

        QString getAbsoluteFn();
        QString getEnding();
        void refresh();
        void edited(int column);

        static std::vector<TFile*> getAllFiles();
        static std::vector<TFile*> &getAllFilesByReference();
};

