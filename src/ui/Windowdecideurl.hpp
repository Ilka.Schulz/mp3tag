#pragma once

#include <QDialog>

class QGridLayout;
class QLabel;
class QListWidget;
class QListWidgetItem;
class QPushButton;

class TWindowDecideUrl : public QDialog  {
    private:
        QString selected_url;
        static int current_instances;

        QGridLayout *rootGrid;
            QLabel *questionLabel;
            QLabel *titleQuestionLabel;
            QLabel *titleLabel;
            QLabel *artistQuestionLabel;
            QLabel *artistLabel;
            QLabel *hintLabel;
            QListWidget *urlListWidget;
            QPushButton *cancelButton;

    public:
        explicit TWindowDecideUrl(QWidget *parent = nullptr);
        ~TWindowDecideUrl();

        static QString execute(QString artist, QString title, QStringList urls);
        static int getCurrentInstances();

    private slots:
        virtual void closeEvent(QCloseEvent *event) override;
        void on_pushButton_Cancel_clicked();
        void on_listWidget_Urls_itemDoubleClicked(QListWidgetItem *item);
};
