/// @file

#pragma once

#include "src/patterns/Observable.hpp"
#include "src/PreferencesObserver.hpp"

class Preferences  :  public Observable<PreferencesObserver>  {
    private:
        static Preferences *instance;
        bool geniusUpdateArtist,
             geniusUpdateAlbum,
             geniusUpdateTitle,
             geniusUpdateYear,
             geniusUpdateGenre,
             geniusUpdateFilename,
             geniusSearchWithArtist,
             geniusSearchWithAlbum,
             geniusSearchWithTitle,
             geniusSearchWithYear,
             geniusSearchWithGenre,
             geniusSearchWithFilename,
             geniusAlwaysUseFirstHit;

    private:
        Preferences();
    public:
        static Preferences *getInstance();
        void informObserver(PreferencesObserver *observer);
    public:
        bool getSetting_geniusUpdateArtist();
        bool getSetting_geniusUpdateAlbum();
        bool getSetting_geniusUpdateTitle();
        bool getSetting_geniusUpdateYear();
        bool getSetting_geniusUpdateGenre();
        bool getSetting_geniusUpdateFilename();
        bool getSetting_geniusSearchWithArtist();
        bool getSetting_geniusSearchWithAlbum();
        bool getSetting_geniusSearchWithTitle();
        bool getSetting_geniusSearchWithYear();
        bool getSetting_geniusSearchWithGenre();
        bool getSetting_geniusSearchWithFilename();
        bool getSetting_geniusAlwaysUseFirstHit();

        void setSetting_geniusUpdateArtist(bool value);
        void setSetting_geniusUpdateAlbum(bool value);
        void setSetting_geniusUpdateTitle(bool value);
        void setSetting_geniusUpdateYear(bool value);
        void setSetting_geniusUpdateGenre(bool value);
        void setSetting_geniusUpdateFilename(bool value);
        void setSetting_geniusSearchWithArtist(bool value);
        void setSetting_geniusSearchWithAlbum(bool value);
        void setSetting_geniusSearchWithTitle(bool value);
        void setSetting_geniusSearchWithYear(bool value);
        void setSetting_geniusSearchWithGenre(bool value);
        void setSetting_geniusSearchWithFilename(bool value);
        void setSetting_geniusAlwaysUseFirstHit(bool value);
};
