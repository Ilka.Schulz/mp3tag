
/// @file

#pragma once

#include <QApplication>

class Application  final  :  public QApplication  {
public:
    Application(int &argc, char **argv);
    virtual bool notify(QObject *sender, QEvent *event)  override;
};
