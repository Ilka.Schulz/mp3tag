#pragma once

/// @file

class Player;
class File;

class PlayerObserver  {
    protected:
        virtual void playerEvent_fileSelected(Player *player);
        virtual void playerEvent_started(Player *player);
        virtual void playerEvent_paused(Player *player);
        virtual void playerEvent_timeStampChanged(Player *player);
        virtual void playerEvent_durationChanged(Player *player);
        virtual void playerEvent_stopped(Player *player);
        virtual void playerEvent_loopModeChanged(Player *player);
        virtual void playerEvent_shuffleModeChanged(Player *player);
        virtual void playerEvent_volumeChanged(Player *player);
    friend Player;
};
