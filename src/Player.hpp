#pragma once

/// @file

#include "src/patterns/Observable.hpp"
#include "PlayerObserver.hpp"
#include <QMediaPlayer>
#include <vector>

class TFile;

class Player  : protected QMediaPlayer, public Observable<PlayerObserver>  {
    public:
        using QMediaPlayer::QObject;
        enum LoopMode  { none, repeatSingle, repeatAll };
    private:
        std::vector<TFile*> &files;
        TFile *currentlyPlayingFile;
        LoopMode loopMode;
        bool shuffleMode;
    public:
        Player(std::vector<TFile*> &_files);
        /// @todo constructors, etc.
    public:
        void selectFile(TFile *file);
        void start();
        void pause();
        void stop();
        void setLoopMode(LoopMode loopMode);
        void setShuffleMode(bool shuffle);
        void setVolume(int volume);
        void moveToTimeStamp(qint64 timeStamp);

        TFile *getCurrentlyPlayingFile();
        LoopMode getLoopMode();
        bool getShuffleMode();
        int getVolume();
        bool isPlaying();
        qint64 getTimeStamp();  /// @returns the current time stamp in milliseconds
        qint64 getDuration();   /// @returns the duration of the currently selected media file in milliseconds

    private:
        // slots
        void stateChanged(State state);
        void positionChanged(qint64 position);
        void durationChanged(qint64 duration);
};
