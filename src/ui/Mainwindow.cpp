/// @file

#include "Mainwindow.hpp"

// ui
#include <QMenuBar>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QToolButton>
#include "src/ui/Treewidget.hpp"
#include <QStatusBar>

#include "qdir.h"
#include "qfiledialog.h"
#include "src/util/Exception.hpp"
#include "iostream"
#include "qprocess.h"
#include "qmenu.h"
#include "qaction.h"
#include "src/File.hpp"
#include <vector>
#include "src/ui/Windowdecideurl.hpp"
#include <QThread>
#include "src/ui/Windowpreferences.hpp"

using namespace std;


TMainWindow::TMainWindow(QWidget *parent) :
        QMainWindow(parent),
        geniusLookupManager(this)  {
    resize(800,600);
    setWindowTitle("FiddlePlayer - by Ilka Schulz");
    QWidget *centralWidget = new QWidget(this);
    setCentralWidget(centralWidget);
    // root grid
    grid = new QGridLayout(centralWidget);
    centralWidget->setLayout(grid);
        // path selection
            // label
            pathLabel = new QLabel(this);
            pathLabel->setText("Path:");
            grid->addWidget(pathLabel, 0,0);
            // line edit
            pathLineEdit = new QLineEdit(this);
            pathLineEdit->setPlaceholderText("/path/to/music/");
            grid->addWidget(pathLineEdit, 0,1);
            connect(pathLineEdit, &QLineEdit::textEdited, this, &TMainWindow::pathLineEditEditedEvent);
            connect(pathLineEdit, &QLineEdit::returnPressed, this, &TMainWindow::pathLineEditReturnPressedEvent);
            // selection tool button
            pathSelectionToolButton = new QToolButton(this);
            pathSelectionToolButton->setText("...");
            grid->addWidget(pathSelectionToolButton, 0,2);
            connect(pathSelectionToolButton, &QToolButton::clicked, this, &TMainWindow::pathSelectionToolButtonClickedEvent);
            // confirmation tool button
            pathConfirmationToolButton = new QToolButton(this);
            pathConfirmationToolButton->setText("OK");
            grid->addWidget(pathConfirmationToolButton, 0,3);
            connect(pathConfirmationToolButton, &QToolButton::clicked, this, &TMainWindow::pathConfirmationToolButtonClickedEvent);
        // tree widget
        treeWidget = new TTreeWidget(this);
        treeWidget->sortByColumn(1,Qt::AscendingOrder);  /// @todo magic number
        treeWidget->setContextMenuPolicy(Qt::ContextMenuPolicy::CustomContextMenu);
        treeWidget->headerItem()->setText(0, tr("Subfolder"));
        treeWidget->headerItem()->setText(1, tr("File"));
        treeWidget->headerItem()->setText(2, tr("Artist"));
        treeWidget->headerItem()->setText(3, tr("Album"));
        treeWidget->headerItem()->setText(4, tr("Title"));
        treeWidget->headerItem()->setText(5, tr("Year"));
        treeWidget->headerItem()->setText(6, tr("Genre"));
        /*treeWidget->setColumnWidth(0,100);
        treeWidget->setColumnWidth(1,100);
        treeWidget->setColumnWidth(2,100);
        treeWidget->setColumnWidth(3,100);
        treeWidget->setColumnWidth(4,100);
        treeWidget->setColumnWidth(5,100);
        treeWidget->setColumnWidth(6,100);*/
        connect(treeWidget, &QTreeWidget::customContextMenuRequested, this, &TMainWindow::treeWidgetCustomContextMenuRequestedEvent);
        connect(treeWidget, &QTreeWidget::itemChanged, this, &TMainWindow::treeWidgetItemChangedEvent);
        grid->addWidget(treeWidget, 1,0, 1,4);
        // player
        playerWidget = new PlayerWidget(this);
        grid->addWidget(playerWidget, 2,0, 1,4);

    // main menu
    {
        /*// File
        QMenu *menuFile = menuBar()->addMenu(tr("&File"));
            // New
            QMenu *menuFileNew = menuFile->addMenu(tr("New"));*/
        // Edit
        QMenu *menuEdit = menuBar()->addMenu(tr("&Edit"));
            // Preferences
            QAction *menuEditPreferences = menuEdit->addAction(tr("&Preferences"));
            connect(menuEditPreferences, &QAction::triggered, this, &TMainWindow::on_actionPreferences_triggered);
    }

    // file system watcher
    connect(&fswatcher, &QFileSystemWatcher::fileChanged, this, &TMainWindow::on_fileChanged);
    connect(&fswatcher, &QFileSystemWatcher::directoryChanged, this, &TMainWindow::on_directoryChanged);

    label_StatusBar_Requests = new QLabel();  /// @todo rename
    statusBar()->addWidget(label_StatusBar_Requests);
    label_StatusBar_QueuedRequests = new QLabel();  /// @todo rename
    statusBar()->addWidget(label_StatusBar_QueuedRequests);
    /// @todo add info about queued requests

    connect(&timer, &QTimer::timeout, this, &TMainWindow::on_timeOut);
    timer.setInterval(10);
    timer.start();

    //ui->lineEditPath->setText(QDir::currentPath());
    selectPath("/home/user/Music/neu6"); /// @todo magic string – store last opened directory instead
    /// @todo maximize window
}
TMainWindow::~TMainWindow()  {
}

void TMainWindow::scanDir()  {
    /// @todo check whether there is unsaved work
    TFile::working_dir = pathLineEdit->text();
    if(TFile::working_dir.right(1) != "/")
        TFile::working_dir += "/";
    pathLineEdit->setText(TFile::working_dir);

    treeWidget->clear();

    QStringList list = fswatcher.files();
    if(!list.empty())
        fswatcher.removePaths(list);
    list = fswatcher.directories();
    if(!list.empty())
        fswatcher.removePaths(fswatcher.directories());

    scanDir(TFile::working_dir);
    treeWidget->resizeColumnToContents(1); /// @todo magic number
}
void TMainWindow::scanDir(QString dn, bool recursive)  {
    //cout << "scanning "<<dn.toUtf8().data()<<endl;
    if(dn.right(1) != "/")
        dn += "/";
    fswatcher.addPath(dn);
    QDir dir(dn);
    dir.setFilter(QDir::AllDirs | QDir::Drives | QDir::Files | QDir::NoDotAndDotDot | QDir::Writable);
    dir.setNameFilters(QStringList()<<"*.mp3");  /// @todo magic string
    QFileInfoList list = dir.entryInfoList();
    for(QFileInfo info : list)  {
        if(info.isDir())  {
            if(recursive)
                scanDir(info.absoluteFilePath());
        }
        else {
            //cout << " * "<<info.fileName().toUtf8().data() << endl;
            QString fn = info.absoluteFilePath();
            bool exists=false;
            for(int i=0; i<treeWidget->topLevelItemCount(); i++)  {
                TFile *f = dynamic_cast<TFile*>(treeWidget->topLevelItem(i));
                if(f->getAbsoluteFn() == fn)  {
                    exists = true;
                    break;
                }
            }
            if(!exists)  {
                new TFile(fn, treeWidget);
                fswatcher.addPath(fn);  // add paths, too
            }
        }
    }
}


void TMainWindow::pathSelectionToolButtonClickedEvent(bool)  {
    QFileDialog d(this);
    d.setFileMode(QFileDialog::Directory);
    d.setOption(QFileDialog::ShowDirsOnly);
    if(d.exec())  {
        QStringList dns = d.selectedFiles();  // always returns absolute paths
        if(dns.size()>1)
            ERROR("cannot open more than one directory at once");
        QString dn = dns[0];
        pathLineEdit->setText(dn);
        scanDir();
    }
}
void TMainWindow::pathLineEditEditedEvent(const QString &arg1)  {
    bool ok = arg1.size()!=0 && QDir(arg1).exists();
    QPalette p = pathLineEdit->palette();
        p.setColor(QPalette::Base,  ok ? Qt::white : QColor(255,128,128));
        pathLineEdit->setPalette(p);
    pathConfirmationToolButton->setEnabled(ok);
}
void TMainWindow::pathLineEditReturnPressedEvent()  { pathConfirmationToolButtonClickedEvent(false); }
void TMainWindow::pathConfirmationToolButtonClickedEvent(bool)  { scanDir(); }
void TMainWindow::treeWidgetCustomContextMenuRequestedEvent(const QPoint &pos)  {
    QMenu menu(this);
        QAction actionPlay("play",this);
            actionPlay.setEnabled(treeWidget->selectedItems().size() > 0);
            connect(&actionPlay, &QAction::triggered, this, &TMainWindow::on_play_triggered);
            menu.addAction(&actionPlay);
        QAction actionPlayWithVlc("play with VLC",this);
            actionPlayWithVlc.setEnabled(treeWidget->selectedItems().size() > 0);
            connect(&actionPlayWithVlc, &QAction::triggered, this, &TMainWindow::on_play_triggered);
            menu.addAction(&actionPlayWithVlc);
        QAction actionEdit("edit with Audacity");
            actionEdit.setEnabled(treeWidget->selectedItems().size() > 0);
            connect(&actionEdit, &QAction::triggered, this, &TMainWindow::on_edit_triggered);
            menu.addAction(&actionEdit);

        menu.addSeparator();

        QAction actionRefresh("refresh",this);
            actionRefresh.setEnabled(treeWidget->selectedItems().size() > 0 );
            connect(&actionRefresh, &QAction::triggered, this, &TMainWindow::on_refresh_triggered);
            menu.addAction(&actionRefresh);
        QAction actionMoveToTrash("move to trash",this);
            actionMoveToTrash.setEnabled(treeWidget->selectedItems().size() > 0 );
            connect(&actionMoveToTrash, &QAction::triggered, this, &TMainWindow::on_moveToTrash_triggered);
            menu.addAction(&actionMoveToTrash);
        QAction actionMoveToDone("move to subdirectory \"done\"",this);
            actionMoveToDone.setEnabled(treeWidget->selectedItems().size() > 0 );
            connect(&actionMoveToDone, &QAction::triggered, this, &TMainWindow::on_moveToDone_triggered);
            menu.addAction(&actionMoveToDone);
        QAction actionMoveToWorkingDir("move to working directory",this);
            actionMoveToWorkingDir.setEnabled(treeWidget->selectedItems().size() > 0 );
            connect(&actionMoveToWorkingDir, &QAction::triggered, this, &TMainWindow::on_moveToWorkingDirectory_triggered);
            menu.addAction(&actionMoveToWorkingDir);
        QAction actionSwitchToSubdir("switch to this sub directory");
            actionSwitchToSubdir.setEnabled(
                        treeWidget->selectedItems().size() == 1
                        && dynamic_cast<TFile*>(treeWidget->selectedItems()[0])->getSubdir().size()!=0 );
            connect(&actionSwitchToSubdir, &QAction::triggered, this, &TMainWindow::on_switchToSubdir_triggered);
            menu.addAction(&actionSwitchToSubdir);

        menu.addSeparator();

        QAction actionSetAccordingFn("choose file name from tags",this);
            actionSetAccordingFn.setEnabled(treeWidget->selectedItems().size()>0);
            connect(&actionSetAccordingFn, &QAction::triggered, this, &TMainWindow::on_setAccordingFn_triggered);
            menu.addAction(&actionSetAccordingFn);
        QAction actionParseFn("parse tags from file name",this);
            actionParseFn.setEnabled(treeWidget->selectedItems().size() > 0);
            connect(&actionParseFn, &QAction::triggered, this, &TMainWindow::on_parseFn_triggered);
            menu.addAction(&actionParseFn);
        QAction actionFindTagsOnline("find tags online on genius.com",this);
            actionFindTagsOnline.setEnabled(treeWidget->selectedItems().size() > 0 );
            connect(&actionFindTagsOnline, &QAction::triggered, this, &TMainWindow::on_findTagsOnline_triggered);
            menu.addAction(&actionFindTagsOnline);
    menu.exec(treeWidget->viewport()->mapToGlobal(pos));
}
void TMainWindow::treeWidgetItemChangedEvent(QTreeWidgetItem *item, int column)  {
    //if(!item->flags().testFlag(Qt::ItemIsEditable))
    //    return ;
    TFile *f = dynamic_cast<TFile*>(item);
    if(f==nullptr)
        ERROR("invalid widget item");
    f->edited(column);
}


void TMainWindow::on_play_triggered()  {
    assert( ! treeWidget->selectedItems().empty() );
    TFile *file = dynamic_cast<TFile*>(treeWidget->selectedItems().at(0));
    assert(file);
    playerWidget->selectFile(file);
    playerWidget->start();
}
void TMainWindow::on_play_with_vlc_triggered()  {
    QString cmd = "vlc --started-from-file ";
    for(QTreeWidgetItem *item : treeWidget->selectedItems())
        cmd  +=  "\"" + dynamic_cast<TFile*>(item)->getAbsoluteFn() + "\" ";
    //QProcess p;
    //p.startDetached(cmd);
    system((cmd+" &").toUtf8().data());
}
void TMainWindow::on_edit_triggered()  {
    for(QTreeWidgetItem *item : treeWidget->selectedItems())  {
        TFile *file = dynamic_cast<TFile*>(item);
        QString cmd = "audacity \"" + file->getAbsoluteFn() + "\"";
        system((cmd+" &").toUtf8().data());
    }
}
void TMainWindow::on_refresh_triggered()  {
    for(QTreeWidgetItem *item : treeWidget->selectedItems())
        dynamic_cast<TFile*>(item)->refresh();
}
void TMainWindow::on_moveToTrash_triggered()  {
    for(QTreeWidgetItem *item : treeWidget->selectedItems())  {
        TFile *f = dynamic_cast<TFile*>(item);
        QProcess p;
        p.start("gio trash \""+f->getAbsoluteFn()+"\"");
        if(!p.waitForFinished(30*1000) || p.exitCode()!=0)
            ERROR("could not move to trash");
        // do not delete, as this is done by the file system watcher
    }
}
void TMainWindow::on_moveToDone_triggered()  {
    QDir donedir(TFile::working_dir+"done/");
    if(!donedir.exists())  {
        if(! QDir(TFile::working_dir).mkdir("done/"))
            ERROR("cannot create subdirectory \"done/\" in current working directory");
        fswatcher.addPath(TFile::working_dir+"done/");
    }
    if(!donedir.exists())
        ERROR("subdiretory has vanished");
    for(QTreeWidgetItem *item : treeWidget->selectedItems())  {
        TFile *file = dynamic_cast<TFile*>(item);
        if(! QFile::rename(file->getAbsoluteFn(), TFile::working_dir+"done/"+file->getFn()) )
            ERROR_MSG("cannot move file >"+file->getAbsoluteFn()+"<");
        file->refresh();
    }
}
void TMainWindow::on_moveToWorkingDirectory_triggered()  {
    for(QTreeWidgetItem *item : treeWidget->selectedItems())  {
        TFile *file = dynamic_cast<TFile*>(item);
        if(file->getSubdir().size()==0)
            continue ;
        if(! QFile::rename(file->getAbsoluteFn(), TFile::working_dir+file->getFn()) )
            ERROR_MSG("cannot move file >"+file->getAbsoluteFn()+"<");
    }
}
void TMainWindow::on_switchToSubdir_triggered()  {
    QString newpath = TFile::working_dir + dynamic_cast<TFile*>(treeWidget->selectedItems()[0])->getSubdir();
    if(! QDir(newpath).exists() || ! QFileInfo(newpath).isWritable())
        ERROR("The directory does not exists or is not writable.");
    pathLineEdit->setText(newpath);
    scanDir();
}
void TMainWindow::on_setAccordingFn_triggered()  {
    for(QTreeWidgetItem *item : treeWidget->selectedItems())  {
        TFile *f = dynamic_cast<TFile*>(item);
        if(f->getTitle().size()==0)
            continue;
        vector<QString> strings{
                    f->getArtist(),
                    f->getAlbum(),
                    f->getTitle(),
                };
        for(vector<QString>::iterator itr=strings.begin(); itr!=strings.end(); itr++)
            if(itr->size()==0)  {
                strings.erase(itr);
                itr--;
            }
        QString newfn = "";
        for(vector<QString>::iterator itr=strings.begin(); itr!=strings.end(); itr++)
            newfn += *itr + (std::next(itr)==strings.end() ? "" : " - ");
        if(f->getYear().size()!=0)
            newfn += " ("+f->getYear()+")";
        if(f->getGenre().size()!=0)
            newfn += " ["+f->getGenre()+"]";
        newfn += f->getEnding();
        f->setFn(newfn);
    }
}
void TMainWindow::on_parseFn_triggered()  {
    /// @todo hourglass cursor
    for(QTreeWidgetItem *item : treeWidget->selectedItems())  {
        TFile *f = dynamic_cast<TFile*>(item);
        QString s = f->getFn();
        QStringList strings = s.split(" - ",QString::SkipEmptyParts);
        s += " ";
        if(strings.size() > 3)  {
            ERROR_MSG("cannot parse file name >"+s+"< - too many parts");
            continue;
        }
        if(strings.size() == 0)
            ERROR("could not split file name");
        if(strings.size()>=2)  {
            f->setArtist(strings[0]);
            strings.removeFirst();
        }
        if(strings.size()==2)  {
            f->setAlbum(strings[0]);
            strings.removeFirst();
        }
        if(strings.size()==1)  {
            QString s = QFileInfo(strings[0]).baseName();
            QString title="", year="", genre="";
            QString *current = &title;
            bool last_was_bracket=false;
            while(s.size() != 0)  {
                if(last_was_bracket)  {
                    while(s.size()!=0 && s[0]==' ')
                        s.remove(0,1);
                }
                last_was_bracket = true;
                QChar c = s[0];
                if(c=='(')  /// @todo only last brackets
                    current = &year;
                else if(c==')')
                    current = &title;
                else if(c=='[')
                    current = &genre;
                else if(c==']')
                    current = &title;
                else  {
                    *current += c;
                    last_was_bracket = false;
                }
                s.remove(0,1);
            }
            if(year.size()!=0)
                f->setYear(year);
            if(genre.size()!=0)
                f->setGenre(genre);
            if(title.size()==0)
                ERROR_MSG("The file name does not include a title");  /// @todo make warning
            if(title[title.size()-1] == ' ')
                title.remove(title.size()-1, 1);
            f->setTitle(title);
        }
    }
}
void TMainWindow::on_findTagsOnline_triggered()  {
    for(QTreeWidgetItem *item : treeWidget->selectedItems()) {
        TFile *f = dynamic_cast<TFile*>(item);
        geniusLookupManager.lookUp(f);
    }
}

// ----------------------------------------------------------------------------
void TMainWindow::on_fileChanged(const QString &path)  {
    for(int i=0; i<treeWidget->topLevelItemCount(); i++)  {
        TFile *f = dynamic_cast<TFile*>(treeWidget->topLevelItem(i));
        if(f->getAbsoluteFn() == path)  {
            f->refresh();
            return ;
        }
    }
    //ERROR("unknown change on hard drive"); renaming files triggeres this event, too.
}
void TMainWindow::on_directoryChanged(const QString &path)  {
    /// @todo check for new or deleted files
    scanDir(path,false);
}
void TMainWindow::on_timeOut()  {
    label_StatusBar_Requests->setText("connections: "+QString::number(geniusLookupManager.getActiveRequestsCount()));
    label_StatusBar_QueuedRequests->setText("queued connections: "+QString::number(geniusLookupManager.getQueuedRequestsCount()));
}
void TMainWindow::on_actionPreferences_triggered()  {
    TWindowPreferences::instance()->show();
}
void TMainWindow::selectPath(QString path)  {
    pathLineEdit->setText(path);
    pathConfirmationToolButtonClickedEvent(false);
}
